import logging

logging.basicConfig(format='%(asctime)s:\033[5;33;31m%(levelname)s\033[0m:%(message)s',datefmt='%d-%m-%Y %I:%M:%S %p')
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
    # logger.info('This is an info message1')
    # logger.error('This is an error message')
    # logger.warning('This is a warning message')
    # logger.critical('This is a critical message')

log_file = []
with open("input/teste03","r") as file:
    for line in file:
        log_file.append(line)

log = []
for line in log_file:
    line = line.replace("<","")
    line = line.replace(">","")
    line = line.replace("\n","")
    line = line.replace("|",",")
    line = line.replace("=",",")
    line = line.replace("(",",")
    line = line.replace(")","")
    line = line.replace(" ",",")

    new_line = line.split("=")
    new_line = line.split("|")
    new_line = line.split(",")

    log.append(new_line)
#log.pop()

dado_temp = log.pop(0)
dados = {}
txn = []
for dado in range(0,len(dado_temp),2):
    dados[dado_temp[dado]] = dado_temp[dado+1]
logger.info("Dados")
print(dados)

print()
logger.info("Logfile")
for line in log:
    print(line)

undo = []
redo = []
commit = []

log.reverse()
e = 0
for line in log:
    
    if len(line) == 4:
        txn.append(line)
        
    elif('start' in line[0]):
        if(['commit',line[1]] in commit and e < 2):
            redo.append(line)
        elif(['commit',line[1]] not in commit):
            undo.append(line)
    elif('End' in line[0] and e == 0): # 1º ckpt encontrado
        e = 1       
    elif('Start' in line[0] and e == 1): # fim do primeiro
        e = e + 1 #e = 2, achou mais que um ckpt'
    # vai pra linha de cima;
    elif('commit' in line[0]):
        commit.append(line)
       
print()
logger.info('Transaction')
for line in txn:
    print(line)

print()
logger.info('Commits')
for line in commit:
    print(line)

print()
logger.info('Undo')
for line in undo:
    print(line)

print()
logger.info('Redo')
for line in redo:
    print(line)
